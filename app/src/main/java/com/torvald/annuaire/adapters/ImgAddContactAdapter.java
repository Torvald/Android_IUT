package com.torvald.annuaire.adapters;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.torvald.annuaire.R;
import com.torvald.annuaire.models.Image;

import java.util.List;

/**
 * Created by Quentin LAURENT on 15/02/2018.
 */

public class ImgAddContactAdapter extends RecyclerView.Adapter<ImgAddContactAdapter.ViewHolder> {

    private List<Image> imageList;
    private int itemLayout;

    public ImgAddContactAdapter(List<Image> imageList, int itemLayout) {
        this.imageList = imageList;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        Image image = imageList.get(position);
        holder.image.setImageResource(image.getImg());
    }

    @Override public int getItemCount() {
        return imageList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;

        ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.contact_img);
        }
    }
}
