package com.torvald.annuaire.models;

import java.io.Serializable;

/**
 * Created by ql269 on 05/02/2018.
 */

public class Contact implements Serializable{
    private String lastName;
    private String firstName;
    private int age;
    private int img;
    private int id;


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public Contact(String lastName, String firstName, int age, int id, int img) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.img = img;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
