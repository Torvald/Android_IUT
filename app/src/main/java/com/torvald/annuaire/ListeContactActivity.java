package com.torvald.annuaire;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.torvald.annuaire.adapters.ContactAdapter;
import com.torvald.annuaire.database.DBContact;
import com.torvald.annuaire.database.DatabaseOpenHelper;
import com.torvald.annuaire.models.Contact;

import java.util.ArrayList;
import java.util.List;

public class ListeContactActivity extends AppCompatActivity {
    private static int REQUEST_CODE = 1;
    private List<Contact> contactList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ContactAdapter mAdapter;

    Contact contact;
    DatabaseOpenHelper databaseOpenHelper = new DatabaseOpenHelper(this);
    DBContact dbContact = new DBContact(databaseOpenHelper);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListeContactActivity.this, AddContactActivity.class);
                ListeContactActivity.this.startActivityForResult(intent, REQUEST_CODE);
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.reycler_view);

        mAdapter = new ContactAdapter(contactList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(
                getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                contact = contactList.get(position);
                Toast.makeText(getApplicationContext(), "Item Clicked " + position, Toast.LENGTH_LONG).show();
                dbContact.delete(contact);
                contactList.remove(position);
                mAdapter.notifyItemRemoved(position);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }
        ));

        getContacts();
    }

    public void getContacts() {
        ArrayList<Contact> contacts = dbContact.getContacts();
        contactList.addAll(contacts);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                // Changement d'activité
                Intent intent = new Intent(ListeContactActivity.this, LoginActivity.class);
                ListeContactActivity.this.startActivity(intent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String lastName = data.getStringExtra("lastName");
                String firstName = data.getStringExtra("firstName");
                Integer age = data.getIntExtra("age", 0);
                Integer image = data.getIntExtra("image", 0);
                Contact contact = new Contact(lastName, firstName,age, 0, image);
                dbContact.insert(contact);
                contactList.add(contact);
                this.mAdapter.notifyItemInserted(contactList.size());
            }
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        mAdapter.notifyItemInserted(contactList.size()+1);
//    }
}
