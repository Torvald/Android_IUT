package com.torvald.annuaire.database;

import android.provider.BaseColumns;

/**
 * Created by torvald on 20/03/18.
 */

public class ContactDatabase {
    public interface Person extends BaseColumns {
        String tableName = "contact";
        String columnLastName = "last_name";
        String columnLastNameType = "TEXT";
        String columnFirstName = "first_name";
        String columnFirstNameType = "TEXT";
        String columnAge = "age";
        String columnAgeType = "INTEGER";
        String columnImg = "img";
        String columnImgType = "INTEGER";
        int num_columnID = 0;
        int num_columnLastName = 1;
        int num_columnFirstName = 2;
        int num_columnAge = 3;
        int num_columnImg = 4;
    }
}
