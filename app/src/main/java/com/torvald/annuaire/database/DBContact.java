package com.torvald.annuaire.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.torvald.annuaire.models.Contact;

import java.util.ArrayList;

/**
 * Created by torvald on 20/03/18.
 */

public class DBContact {

    private SQLiteOpenHelper sqLiteOpenHelper;

    public DBContact(SQLiteOpenHelper sqLiteOpenHelper) {
        this.sqLiteOpenHelper = sqLiteOpenHelper;
    }

    public void insert(Contact contact) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ContactDatabase.Person.columnLastName, contact.getLastName());
        values.put(ContactDatabase.Person.columnFirstName, contact.getFirstName());
        values.put(ContactDatabase.Person.columnAge, contact.getAge());
        values.put(ContactDatabase.Person.columnImg, contact.getImg());

        db.insert(ContactDatabase.Person.tableName, null, values);
    }

    public void update(Contact contact) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ContactDatabase.Person.columnLastName, contact.getLastName());
        values.put(ContactDatabase.Person.columnFirstName, contact.getFirstName());
        values.put(ContactDatabase.Person.columnAge, contact.getAge());
        values.put(ContactDatabase.Person.columnImg, contact.getImg());

        String selection = ContactDatabase.Person._ID + " = ?";
        String[] selectionArgs = {String.valueOf(contact.getId())};

        db.update(ContactDatabase.Person.tableName, values, selection, selectionArgs);
    }

    public void delete(Contact contact) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        String selection = ContactDatabase.Person._ID + " = ?";
        String[] selectionArgs = {String.valueOf(contact.getId())};

        db.delete(ContactDatabase.Person.tableName, selection, selectionArgs);
    }

    private Cursor getAllContacts() {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        return db.query(ContactDatabase.Person.tableName, null, null, new String[]{}, null, null, null);
    }

    public ArrayList<Contact> getContacts() {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Cursor cursor = getAllContacts();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            contacts.add(new Contact(   cursor.getString(ContactDatabase.Person.num_columnLastName),
                                        cursor.getString(ContactDatabase.Person.num_columnFirstName),
                                        cursor.getInt(ContactDatabase.Person.num_columnAge),
                                        cursor.getInt(ContactDatabase.Person.num_columnID),
                                        cursor.getInt(ContactDatabase.Person.num_columnImg)));
            cursor.moveToNext();
        }

        cursor.close();
        return contacts;
    }
}
